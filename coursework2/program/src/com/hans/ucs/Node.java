package com.hans.ucs;

/**
 * @author Hans-Nikolai Viessmann
 * @version 1.0
 * 
 * Contains all details about a given city
 */
public class Node {

	public int city;
	public String name;
	public int distance;
	public int level;
	/**
	 * A list of all parents, or previous Nodes defining the current path
	 */
	public Node parent = null;

	/**
	 * Instantiate the Node
	 * 
	 * @param city
	 *                 unique id of city
	 * @param name
	 *                 name of the city
	 * @param distance
	 *                 distance from the previous Node
	 * @param level
	 *                 given a tree, which level the Node is from
	 */
	public Node(int city, String name, int distance, int level){
		this.city = city;
		this.name = name;
		this.distance = distance;
		this.level = level;
	}

	/**
	 * @return String: name(city)(Llevel): distance
	 *                 e.g. Edinburgh(2)(L0): 0
	 */
	public String toString(){
		return this.name + "(" + this.city + ")(L" + this.level + "): " + this.distance;
	}
}
