package com.hans.ucs;

import java.util.Comparator;

/**
 * @author Hans-Nikolai Viessmann
 * @version 1.0
 * 
 * Override the compare method in Comparator class
 */
public class Queue implements Comparator<Node> {

	/**
	 * Override the compare method to work correctly
	 * with the Node class.
	 * 
	 * @param city1
	 *              item one to be compared
	 * @param city2
	 *              item two to be compared
	 * @return whether city1 is greater-than (1), 
	 *         equal-to (0), or less-than (-1) city2
	 */
	@Override
	public int compare(Node city1, Node city2) {
		if(city1.distance < city2.distance)
			return -1;
		else if(city1.distance > city2.distance)
			return 1;
		return 0;
	}

}
