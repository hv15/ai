package com.hans.ucs;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.PriorityQueue;

/**
 * @author Hans-Nikolai Viessmann
 * Based on the source code available at:
 * http://code.google.com/p/tspuib/source/browse/trunk/TravelingSalesMan/src/travelingsalesman/UniformCost.java
 */
public class UCSCycle {

	private static int numberOfCities = 8;
	private static int startCity = 0; // Edinburgh
	private static String[] cityName = {
		"Edinburgh",
		"Aberdeen",
		"Ayr",
		"Fort Williams",
		"Glasgow",
		"Inverness",
		"StAndrews",
		"Stirling"
	};
	private static int[][] distance = new int[numberOfCities][numberOfCities];
	private static ArrayList<Integer> optimumRoute; // final path (meant to be best)
	private static ArrayList<Integer> followedRoute; // Random-storage

	/**
	 * Specific Uniform-Cost Search Algorithm for the Travelling Salesman Problem
	 * Based on the source code found at:
	 * http://code.google.com/p/tspuib/source/browse/trunk/TravelingSalesMan/src/travelingsalesman/UniformCost.java
	 * 
	 * Using a priority queue and two array lists, for each level in the search tree
	 * take any one node, add it to an array list of nodes of the current path.
	 * Add all of its branch nodes to the priority queue with total distance from
	 * the start city (Edinburgh). Each branch node implements a linked list like structure,
	 * pointing to its parent node. This list contains the full sequence of previous nodes
	 * defining the path. If the current node is the goal (start city), end loop. Else for
	 * each the given node look at all possible branch nodes. Ensure that it has not been
	 * visited, and also that it has not exceeded the max number of nodes (in this case 8).
	 * Create a new node, and assign its parent to the list. Add to the priority queue.
	 * Continue the loop. 
	 * @param start
	 */
	private static void UCS(Node start){
		boolean found = false;
		PriorityQueue<Node> fringe = new PriorityQueue<Node>(100, new Queue());
		fringe.add(start); // Initialise the queue with the start city (Edinburgh)
		while(!fringe.isEmpty() && !found){
			Node node = fringe.remove();
			Node aux = node;
			
			followedRoute = new ArrayList<Integer>();
			followedRoute.add(node.city);

			//System.out.printf("Current cost: %d%n", node.distance);
			while (aux.level != 0) {
				aux = aux.parent;
				followedRoute.add(aux.city);
				//System.out.println("the level of city "+aux.city+" is not zero");
			}

			if(node.level == numberOfCities){
				found = true;
				optimumRoute = followedRoute;
				System.out.print("Found Path: ");
				printResult(optimumRoute);
			} else {
				for(int i = 0; i < numberOfCities; i++){
					// have we visited this city in the current followed route?
					boolean visited = followedRoute.contains(i);
					boolean isSolution = (followedRoute.size() == numberOfCities) && (i == startCity);

					if(!visited || isSolution){

						Node childTown = new Node(i, node.name, node.distance + distance[node.city][i], node.level + 1);
						childTown.parent = node;
						fringe.offer(childTown);
					}
				}
				//printObjArray(fringe.toArray());
				//printObjArray(visited.toArray());
			}
		}
	}

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length != 1){
			System.out.println("Missing distances.txt file!!!!");
			System.exit(1);
		}
		readTheDistanceMatrix(args[0]);
		//printDistances();
		
		// Create the first node, and assign it a distance of 0 and level of 0
		Node start = new Node(startCity, cityName[startCity], distance[startCity][startCity], 0);
		UCS(start);
	}

	/**
	 * ###TAKEN FROM AI LAB SOURCE CODE###
	 * @param filePath
	 */
	private static void readTheDistanceMatrix(String filePath) {

		int city1, city2;
		BufferedReader readbuffer = null;
		String strRead;
		String splitarray[];
		int inputNumber;

		try {
			System.out.println("Reading Cities from " + filePath);
			readbuffer = new BufferedReader(new FileReader(filePath));
			for (city1 = 0; city1 < numberOfCities; city1++) {
				strRead = readbuffer.readLine();
				splitarray = strRead.split("\t");
				for (city2 = 0; city2 < numberOfCities; city2++) {
					inputNumber = Integer.parseInt(splitarray[city2]);
					distance[city1][city2] = inputNumber;
				}
				//System.out.println("\tRead in city " + city1 + ": " + cityName[city1]);
			}
		} catch (Exception e) {
			System.out.println(e);
			System.exit(0);
		}
	}

	public static void printDistances() {
		System.out.println("City Distances:");
		for(int city1 = 0; city1 < numberOfCities; city1++){
			for(int city2 = 0; city2 < numberOfCities; city2++){
				System.out.printf("%14s - %3d - %-14s%n", cityName[city1], distance[city1][city2], cityName[city2]);
			}
		}
	}

	public static void printObjArray(Object[] objects) {
		System.out.print("\tPrinting OBJ Array: ");
		for(int y = 0; y < objects.length; y++){
			Node city = (Node) objects[y];
			System.out.printf("{%s: %d - %d} ", city.name, city.city, city.distance);
		}
		System.out.println();
	}

	public static void printIntArrayList(ArrayList<Integer> object) {
		System.out.print("\tPrinting INT ArrLs: ");
		for(int y = 0; y < object.size(); y++){
			System.out.printf("{%s: %d} ", cityName[object.get(y)], object.get(y));
		}
		System.out.println();
	}

	public static void printResult(ArrayList<Integer> object) {
		int cost = 0;
		for(int y = object.size() - 1; y > 0; y--){
			System.out.printf("%s(%d) %d ", cityName[object.get(y)], object.get(y), distance[object.get(y)][object.get(y - 1)]);
			cost += distance[object.get(y)][object.get(y - 1)];
		}
		System.out.printf("%s(%d)", cityName[object.get(object.size() - 1)], object.get(object.size() - 1));
		System.out.println();
		System.out.println("Total Cost: " + cost);
	}

	public static void printIntArray(int[] object) {
		System.out.print("\tPrinting INT Array: ");
		for(int y = 0; y < object.length; y++){
			System.out.printf("{%s: %d} ", cityName[object[y]], object[y]);
		}
		System.out.println();
	}

	public static void printStringArray(String[] array) {
		System.out.print("\tPrinting STR Array: ");
		for(int y = 0; y < array.length; y++){
			System.out.printf("{%s} ", array[y]);
		}
		System.out.println();
	}

	public static void printListList(ArrayList<Object[]> list){
		System.out.println("\tPrinting LST Lists:");
		for(Object[] item : list){
			printObjArray(item);
		}
	}

	public static void printArrayList(ArrayList<String> array) {
		System.out.print("\tPrinting LST Array: ");
		for(int y = 0; y < array.size(); y++){
			System.out.printf("{%s} ", array.get(y));
		}
		System.out.println();
	}
}